# Git Workflows
## Basic scenario
![](img/basic.png)
* Someone initializes the remote repository (in a local Git server, on GitHub, BitBucket, or so on)
* Other team members clone the original repository on their computer and start working
* When the work is done, you push it to the remote to make it available to other colleagues

At this point, it is only a matter of internal rules and patterns.

## Feature branch workflow
![](img/git_featrure_branch.png)

## Gitflow
Vincent Driessen

[A successful git branching model - blogpost](http://nvie.com/posts/a-successful-git-branching-model)

use of some main branches

![](img/gitflow.png)

### Master branch
* represents the final stage
* merging your work in it is equal to making a new release of your software
* don't start new branches from the master (only instant fixes)
* You tag your release here

### Hotfix branches
* derived only from the master
* Merge it into develop
* Delete it
* Git grouping branches tipp:
* Use 
`hotfix/<branch name> or hotfix/#123` 
    * where #123 is the incident number

### Develop branch
* is a sort of staging branch
* When starting new feature you need to create new branch from develop
* After task completion you can merge back to develop
* Use `feature/<branch name>` as a tipp

### Release branch
* Use release/<branch name> prefix
* Create new branch from develop
* No new features on release branch, because you need to wrap up the release.
* In case you need to fix a bug in a specific release and it is relevant for future releases don’t forget to merge it into develop

## Exercise Time
Head over to this repo and join the fun.
[Gitflow exercise repo.](https://gitlab.com/tjozsa/git-flow-exercise)

## GitHub flow
* Scott Chacon
* http://scottchacon.com/2011/08/31/github-flow.html
* is better tailored for frequent releases …
* … very frequently, even twice a day
* Continuous Delivery
![](img/githubflow.png)

### What are the key differences compared to Gitflow
* Anything in the master branch is deployable
* Creating descriptive branches off of master
* Pushing to named branches constantly
* Opening a pull request at any time
* Merging only after pull request review
* Deploying immediately after review
