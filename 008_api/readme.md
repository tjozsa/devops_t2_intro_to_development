# What is a web service?
* The W3C defines a "Web service" as "a software system designed to support interoperable machine-to-machine interaction over a network". 
* In a Web service, Web technology such as the HTTP, originally designed for human-to-machine communication, is utilized for machine-to-machine communication, more specifically for transferring machine readable file formats such as XML and JSON. 
* Types:
    * SOAP
    * RESTful 

# HTTP
* HTTP is connectionless
* HTTP is media independent
* HTTP is stateless
* HTTP Version (HTTP/1.0) 
* Uniform Resource Identifiers
* Character Sets (US-ASCII) 
* Content Encodings (Accept-encoding: gzip) 
* Media Types (application/json) 
* Header Fields 
* Message Body 
* Response code

# HTTP headers 
![](img/headers.png)

# HTTP methods 
* **GET** - to retrieve information from the given server using a given URI. Requests using GET should only retrieve data and should have no other effect on the data 
* HEAD - Same as GET, but it transfers the status line and the header section only 
* **POST** - A POST request is used to send data to the server using HTML forms 
* **PUT** - Replaces all the current representations of the target resource with the uploaded content 
* **DELETE** - Removes all the current representations of the target resource given by URI 
* CONNECT - Establishes a tunnel to the server identified by a given URI 
* OPTIONS - Describe the communication options for the target resource 
* TRACE - Performs a message loop back test along with the path to the target resource 
* PATCH - The PATCH method applies partial modifications to a resource

# REST
* REST stands for Representational State Transfer. (It is sometimes spelled "ReST".) It relies on a stateless, client-server, cacheable communications protocol -- and in virtually all cases, the HTTP protocol is used. REST is an architecture style for designing networked applications. 
* REST is an architecture style for designing networked applications. The idea is that, rather than using complex mechanisms such as CORBA, RPC or SOAP to connect between machines, simple HTTP is used to make calls between machines.

# REST demo
* JSONPlaceholder: https://jsonplaceholder.typicode.com/
* National Digital Forecast Database (NDFD) REST Web Service: http://graphical.weather.gov/xml/rest.php
* Narodowy Bank Polski: http://api.nbp.pl/en.html

# SOAP
* SOAP stands for Simple Object Access Protocol 
* SOAP is an application communication protocol 
* SOAP is a format for sending and receiving messages 
* SOAP is platform independent 
* SOAP is based on XML 
* SOAP is a W3C recommendation

WSDL
* Web Services Description Language (WSDL, pronounced 'wiz-dul') is an XML-based language that is used for describing the functionality offered by a Web service. A WSDL description of a web service (also referred to as a WSDL file) provides a machine-readable description of how the service can be called, what parameters it expects and what data structures it returns. WSDL stands for Web Services Description Language 
* WSDL is used to describe web services 
* WSDL is written in XML 
* WSDL is a W3C recommendation

# WSDL parts 
* Data types definition (types) – the definition of sent and received XML service messages. 
* Data elements (message) – message used by web-service. 
* Abstract operations (portType) – the list of operations that can be performed with messages. 
* Service binding (binding) – the way the message will be delivered

# WSDL examples
* ISBN Test: http://webservices.daehosting.com/services/isbnservice.wso?WSDL
* Country Information: http://webservices.oorsprong.org/websamples.countryinfo/CountryInfoService.wso?WSDL
* Number Conversion: http://www.dataaccess.com/webservicesserver/numberconversion.wso?WSDL
* Bankleitzahlen Service: http://www.thomas-bayer.com/axis2/services/BLZService?wsdl 
* National Digital Forecast Database (NDFD) SOAP Web Service: http://graphical.weather.gov/xml/
