# Java is a ...
* Interpreted...
* Strongly typed...
* C-like...
* Object Oriented...
* Multithreaded...
* Enterprise Grade...
* Programming language

# Source Code
* Human readable
* Needs to be compiled... into Java Byte Code

Hello World:
```java
// Single-line comments start with //

/*
Multi-line comments look like this.
*/

/**
* Java Doc comments look like this.
*
* You must have a public class with a runnable method.
*
* This code should be stored in a HelloWorld.java file.
* It will be compiled into a HelloWorld.class file.
*/
public class HelloWorld {
    // In order to run a java program, it must have a main method as an entry
    // point.
    public static void main(String[] args) {
        System.out.println("Hello, World!"));
    }
}
```

# Compile Java Code into Byte Code
In order to compile the above code you must have 
* a HelloWorld.java file
* Java JDK installed
    * `java -version`
    * `javac -version`

```bash
cd where/your/file/is
javac HelloWorld.java
```

# Virtual machine
* Is written in C/C++ 
* Usually implemented for all major Operating Systmes and CPU architectures
* Reads Java Byte Code and runs programs
* One system can run multiple VM-s
* One VM can run multiple threads and applications.

# Run Java Byte Code
```bash
cd /where/your/class/files/are
java HelloWorld
```